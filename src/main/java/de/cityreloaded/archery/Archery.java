package de.cityreloaded.archery;

import de.cityreloaded.archery.arena.Arena;
import de.cityreloaded.archery.arena.ArenaManager;
import de.cityreloaded.archery.command.HelpCommand;
import de.cityreloaded.archery.command.play.JoinCommand;
import de.cityreloaded.archery.command.play.ListCommand;
import de.cityreloaded.archery.command.play.StartCommand;
import de.cityreloaded.archery.command.play.StopCommand;
import de.cityreloaded.archery.command.setup.CreateCommand;
import de.cityreloaded.archery.command.setup.FinishSetupCommand;
import de.cityreloaded.archery.command.setup.SetLobbyCommand;
import de.cityreloaded.archery.listener.ArenaDropItemListener;
import de.cityreloaded.archery.listener.ArenaTargetListener;
import de.cityreloaded.archery.listener.ArenaItemListener;
import de.cityreloaded.archery.listener.ArenaProjectileListener;
import de.cityreloaded.archery.persistence.ArenaStorage;
import de.paxii.bukkit.commandapi.CommandApi;

import org.bukkit.ChatColor;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.IOException;
import java.util.List;
import java.util.stream.Stream;

import lombok.Getter;

public class Archery extends JavaPlugin {

  public static final String CHAT_PREFIX = ChatColor.GOLD + "[Archery] " + ChatColor.RESET;

  @Getter
  private static Archery instance;

  private ArenaManager arenaManager;

  @Getter
  private CommandApi commandApi;

  private boolean saveOnExit = true;

  public Archery() {
    Archery.instance = this;
    this.arenaManager = new ArenaManager();
    this.commandApi = new CommandApi();
  }

  public static ArenaManager getArenaManager() {
    return Archery.instance.arenaManager;
  }

  @Override
  public void onEnable() {
    if (!this.getDataFolder().exists()) {
      this.getDataFolder().mkdir();
    }
    this.commandApi.setChatPrefix(CHAT_PREFIX);
    Stream.of(
            new HelpCommand(),

            new ListCommand(),
            new JoinCommand(),
            new StartCommand(),
            new StopCommand(),

            new CreateCommand(),
            new SetLobbyCommand(),
            new FinishSetupCommand()
    ).forEach(command -> this.commandApi.addCommand(command));
    Stream.of(
            new ArenaTargetListener(),
            new ArenaItemListener(),
            new ArenaProjectileListener(),
            new ArenaDropItemListener()
    ).forEach(listener -> this.getServer().getPluginManager().registerEvents(listener, this));
    this.getCommand("archery").setExecutor(this.commandApi.getExecutor());

    this.getServer().getScheduler().runTaskLater(this, () -> {
      try {
        List<Arena> arenaList = ArenaStorage.loadArenas();
        this.arenaManager.addAll(arenaList);
      } catch (IOException e) {
        this.saveOnExit = false;
        e.printStackTrace();
        System.out.println("WARNING: Could not load Arenas. Won't save Arenas on shutdown.");
      }
    }, 20L * 3);
  }

  @Override
  public void onDisable() {
    this.commandApi.getCommandManager().getCommandList().clear();
    // TODO: unregister listeners

    if (this.saveOnExit) {
      try {
        ArenaStorage.saveArenas(this.arenaManager.getAll());
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }

}
