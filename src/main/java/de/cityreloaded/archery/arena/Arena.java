package de.cityreloaded.archery.arena;

import de.cityreloaded.archery.Archery;
import de.cityreloaded.archery.inventory.InventoryManager;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import lombok.Getter;
import lombok.Setter;

public class Arena implements Comparable<Arena> {

  /**
   * Name of the Arena
   */
  @Getter
  protected String name;

  /**
   * Total payment this arena will award when hitting all targets
   */
  @Getter
  @Setter
  protected int payment;

  /**
   * Total amount of arrows a player can shoot
   */
  @Getter
  @Setter
  protected int allowedAttempts = 10;

  /**
   * The spawn of the Arena. Used for all players.
   */
  @Getter
  @Setter
  protected Location spawn;

  /**
   * The lobby of the Arena. Used for all players.
   */
  @Getter
  @Setter
  protected Location lobby;

  /**
   * The target area that contains the bull's eye TODO add add and remove methods instead of
   * exposing the list
   */
  @Getter
  protected ArrayList<Target> targetList;

  /**
   * Is true if this arena is currently being set up
   */
  @Getter
  @Setter
  protected boolean inSetup;

  /**
   * Is true if this arena was already started
   */
  @Getter
  protected boolean started;

  /**
   * The minimum amount of players required to start this arena
   */
  @Getter
  @Setter
  protected int minimumPlayers = 1;

  /**
   * The maximum amount of players allowed in this arena
   */
  @Getter
  @Setter
  protected int maximumPlayers = 30;

  /**
   * List of players currently playing in this arena
   */
  protected HashMap<UUID, Attempt> activePlayers;

  /**
   * InventoryManager used to store and restore player's inventories on start and end, respectively
   */
  protected InventoryManager inventoryManager;

  public Arena(String name, int payment) {
    this(name, payment, null, new ArrayList<>());
  }

  public Arena(String name, int payment, Location spawn) {
    this(name, payment, spawn, new ArrayList<>());
  }

  public Arena(String name, int payment, Location spawn, ArrayList<Target> targetList) {
    this.name = name;
    this.payment = payment;
    this.spawn = spawn;
    this.targetList = targetList;
    this.activePlayers = new HashMap<>();
    this.inventoryManager = new InventoryManager();
  }

  /**
   * @param player The player to check
   * @return true if the given player is currently playing in this Arena
   */
  public boolean isPlaying(OfflinePlayer player) {
    return this.isPlaying(player.getUniqueId());
  }

  /**
   * @param player the player to check
   * @return true if the given player is currently playing in this Arena
   */
  public boolean isPlaying(UUID player) {
    return this.activePlayers.containsKey(player);
  }

  /**
   * @param player The player to add to the list of active players
   */
  public boolean addPlayer(OfflinePlayer player) {
    if (this.getPlayerCount() < this.maximumPlayers) {
      this.activePlayers.put(player.getUniqueId(), new Attempt(player));
      this.sendMessages(String.format("%s joined the Arena!", player.getName()));
      return true;
    }

    return false;
  }

  /**
   * @param player The player to remove from the list of active players
   */
  public void removePlayer(OfflinePlayer player) {
    this.activePlayers.remove(player.getUniqueId());
  }

  /**
   * @return The amount of players currently participating
   */
  public int getPlayerCount() {
    return this.activePlayers.size();
  }

  /**
   * @param player the player to get the attemtps for
   * @return the attempts of the player
   */
  public Attempt getAttempt(OfflinePlayer player) {
    return this.getAttempt(player.getUniqueId());
  }

  /**
   * @param player the player to get the attemtps for
   * @return the attempts of the player
   */
  public Attempt getAttempt(UUID player) {
    return this.activePlayers.get(player);
  }

  /**
   * @return true if this arena can be started, false otherwise
   */
  public boolean canStart() {
    return !this.started
            && this.spawn != null
            && this.lobby != null
            && this.targetList.size() > 0
            && this.getPlayerCount() >= this.minimumPlayers;
  }

  /**
   * @return true if the arena was started, false otherwise
   */
  public boolean start() {
    if (this.canStart()) {
      ItemStack bow = new ItemStack(Material.BOW, 1);
      ItemStack arrows = new ItemStack(Material.ARROW, this.allowedAttempts);
      ItemMeta itemMeta = bow.getItemMeta();
      itemMeta.setDisplayName(ChatColor.LIGHT_PURPLE + "Eventbow");
      bow.setItemMeta(itemMeta);
      this.getOnlinePlayers().forEach(player -> {
        this.inventoryManager.storeInventory(player);
        player.getInventory().clear();
        player.getInventory().setArmorContents(new ItemStack[4]);
        player.teleport(this.spawn);
        player.getInventory().addItem(bow);
        player.getInventory().addItem(arrows);
      });
      this.sendMessages("The Arena was started. Have fun!");
      this.started = true;

      return true;
    }

    return false;
  }

  /**
   * @return true if the arena can be stopped
   */
  public boolean canStop() {
    return this.started
            && this.activePlayers.values().stream().filter(attempt -> attempt.getUsedAttempts() < this.allowedAttempts).count() == 0;
  }

  /**
   * @return true if the arena was stopped, false otherwise
   */
  public boolean stop() {
    if (this.canStop()) {
      // TODO: restore inventory for players disconnecting
      this.getOnlinePlayers().forEach(player -> {
        player.teleport(this.lobby);
        this.inventoryManager.restoreInventory(player);
        this.targetList.forEach(target -> target.resetBlocksFor(player));
        Attempt attempt = this.getAttempt(player);
        this.sendMessage(player, String.format("You hit %d bull's eyes and %d blocks!", attempt.getBullsEyeHits(), attempt.getBlockHits()));
        // FIXME: Implement properly, move to "ArenaFinisher"?
        int points = (attempt.getBullsEyeHits() * 10) + (attempt.getBlockHits() * 5);
        this.sendMessage(player, String.format("You reached %d points!", points));

        if (points < 10) {
          this.sendMessage(player, "You didn't earn enough points to get any reward. :(");
        } else {
          ArrayList<ItemStack> rewards = new ArrayList<>(10);

          if (points > 10) {
            rewards.add(new ItemStack(Material.WOOD, 32));
          }
          if (points > 20) {
            rewards.add(new ItemStack(Material.STONE, 64));
          }
          if (points > 30) {
            rewards.add(new ItemStack(Material.GLOWSTONE, 12));
          }
          if (points > 40) {
            rewards.add(new ItemStack(Material.IRON_INGOT, 12));
          }
          if (points > 50) {
            ItemStack leatherChestplate = new ItemStack(Material.LEATHER_CHESTPLATE, 1);
            LeatherArmorMeta armorMeta = (LeatherArmorMeta) leatherChestplate.getItemMeta();
            armorMeta.setColor(Color.fromRGB(255, 0, 136));
            armorMeta.setDisplayName(ChatColor.LIGHT_PURPLE + "Event-Shirt");
            leatherChestplate.setItemMeta(armorMeta);
            rewards.add(leatherChestplate);
          }
          if (points > 60) {
            rewards.add(new ItemStack(Material.CARROT, 20));
          }
          if (points > 70) {
            rewards.add(new ItemStack(Material.EXP_BOTTLE, 32));
          }
          if (points > 80) {
            rewards.add(new ItemStack(Material.GOLD_BLOCK, 5));
          }
          if (points > 90) {
            Potion potion = new Potion(PotionType.STRENGTH, 2, false, false);
            rewards.add(potion.toItemStack(3));
            ItemStack bow = new ItemStack(Material.BOW, 1);
            bow.addEnchantment(Enchantment.ARROW_DAMAGE, 5);
            bow.addEnchantment(Enchantment.ARROW_INFINITE, 1);
            bow.addEnchantment(Enchantment.DURABILITY, 3);
            bow.addEnchantment(Enchantment.ARROW_FIRE, 1);
            ItemMeta itemMeta = bow.getItemMeta();
            itemMeta.setDisplayName(ChatColor.MAGIC + "!" + ChatColor.RESET + ChatColor.GOLD + "Gewinnerbogen" + ChatColor.RESET + ChatColor.MAGIC + "!");
            bow.setItemMeta(itemMeta);
          }
          if (points == 100) {
            Bukkit.broadcastMessage(Archery.CHAT_PREFIX + String.format("%s gewinnt Elite.", player.getName()));
          }

          rewards.forEach(itemStack -> player.getInventory().addItem(itemStack));
        }
      });
      this.sendMessages("Thank you for playing!");
      this.started = false;
      this.reset();

      return true;
    }

    return false;
  }

  public boolean canFinish() {
    return this.spawn != null
            && this.lobby != null
            && this.targetList.size() > 0;
  }

  public void finish() {
    this.inSetup = false;
  }

  /**
   * Resets activePlayers and the inventoryManager
   */
  private void reset() {
    this.activePlayers = new HashMap<>();
    this.inventoryManager = new InventoryManager();
  }

  /**
   * @return a list of all currently active players
   */
  protected List<Player> getOnlinePlayers() {
    return this.activePlayers
            .keySet()
            .stream()
            .map(Bukkit::getPlayer)
            .collect(Collectors.toCollection(ArrayList::new));
  }

  /**
   * @param location
   * @return
   */
  public Target getTargetByLocation(Location location) {
    return this.targetList.stream()
            .filter(target -> target.contains(location))
            .findFirst().orElse(null);
  }

  /**
   * Send message to all players in this arena
   *
   * @param message message to send to all players
   */
  public void sendMessages(String message) {
    this.getOnlinePlayers().forEach(player -> this.sendMessage(player, message));
  }

  public void sendMessage(Player player, String message) {
    player.sendMessage(Archery.CHAT_PREFIX + message);
  }

  @Override
  public int compareTo(Arena otherArena) {
    return this.name.compareToIgnoreCase(otherArena.name);
  }
}
