package de.cityreloaded.archery.arena;

import org.bukkit.OfflinePlayer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

public class ArenaManager {

  private List<Arena> arenaList;

  public ArenaManager() {
    this.arenaList = new ArrayList<>();
  }

  /**
   * @param arenaName arena name to check for
   * @return true if there is already an arena registered with that name, false otherwise
   */
  public boolean isArenaRegistered(String arenaName) {
    return this.arenaList.stream().filter(arena -> arena.getName().equalsIgnoreCase(arenaName)).count() > 0;
  }

  /**
   * @param player player to check the arenas for
   * @return true if the player is currently in an arena
   */
  public boolean isPlayingInArena(OfflinePlayer player) {
    return this.arenaList.stream()
            .map(Arena::getOnlinePlayers)
            .flatMap(Collection::stream)
            .filter(onlinePlayer -> onlinePlayer.getUniqueId().equals(player.getUniqueId()))
            .count() > 0;
  }

  /**
   * @param arena registers a new arena
   * @return true if there was no arena registered with that name yet, false otherwise
   */
  public boolean registerArena(Arena arena) {
    if (!this.isArenaRegistered(arena.getName())) {
      this.arenaList.add(arena);
      return true;
    }

    return false;
  }

  /**
   * @param arenaName name of the arena to fetch
   * @return the arena or null if none was found
   */
  public Arena getArena(String arenaName) {
    return this.getArena(arenaName, false);
  }

  /**
   * @param arenaName  name of the arena to fetch
   * @param setupState whether or not arenas currently in setup should be returned
   * @return the arena or null if none was found
   */
  public Arena getArena(String arenaName, boolean setupState) {
    Optional<Arena> first = this.arenaList.stream()
            .filter(arena -> arena.getName().equalsIgnoreCase(arenaName))
            .filter(arena -> arena.isInSetup() == setupState)
            .findFirst();

    return first.orElse(null);
  }

  /**
   * @param player player to search the arenas for
   * @return the Arena the player is currently in or null
   */
  public Arena getArenaByPlayer(OfflinePlayer player) {
    return this.getArenaByPlayer(player.getUniqueId());
  }

  public Arena getArenaByPlayer(UUID player) {
    return this.getAll().stream()
            .filter(arena -> arena.isPlaying(player))
            .findFirst().orElse(null);
  }

  /**
   * @return all arenas currently not in setup
   */
  public List<Arena> getAll() {
    return this.getAll(false);
  }

  /**
   * @param setupState whether or not arenas currently in setup should be returned
   * @return all arenas matching the setupState
   */
  public List<Arena> getAll(boolean setupState) {
    return this.arenaList.stream()
            .filter(arena -> arena.isInSetup() == setupState)
            .sorted()
            .collect(Collectors.toCollection(ArrayList::new));
  }

  /**
   * @param arenas list of arenas to add
   */
  public void addAll(List<Arena> arenas) {
    this.arenaList.addAll(arenas);
  }

}
