package de.cityreloaded.archery.arena;

import org.bukkit.OfflinePlayer;

import java.util.ArrayList;
import java.util.UUID;

import lombok.Getter;

public class Attempt {

  @Getter
  private UUID player;
  @Getter
  private int bullsEyeHits;
  @Getter
  private int usedAttempts;
  @Getter
  private int blockHits;
  @Getter
  private ArrayList<Target> finishedTargets;

  public Attempt(OfflinePlayer player) {
    this(player.getUniqueId());
  }

  public Attempt(UUID player) {
    this.player = player;
    this.finishedTargets = new ArrayList<>(10);
  }

  public int addAttempt() {
    return ++this.usedAttempts;
  }

  public int addBullsEyeHit(Target target) {
    this.finishedTargets.add(target);
    return ++this.bullsEyeHits;
  }

  public int addBlockHit(Target target) {
    this.finishedTargets.add(target);
    return ++this.blockHits;
  }
}
