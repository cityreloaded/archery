package de.cityreloaded.archery.arena;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import lombok.Data;

@Data
public class Target {

  private Location bullsEye;
  private List<Location> blocks;
  private byte defaultBullsEyeDamageValue = 14;
  private byte defaultBlockDamageValue = 11;
  private byte defaultFinishedBlockDamageValue = 5;

  public Target(Location bullsEye) {
    this.bullsEye = bullsEye;
    this.blocks = this.findBlocks();
  }

  public Target(Location bullsEye, List<Location> blocks) {
    this.bullsEye = bullsEye;
    this.blocks = blocks;
  }

  public List<Location> findBlocks() {
    Location bullsEye = this.bullsEye.clone();
    ArrayList<Location> possibleLocations = new ArrayList<>();

    int min = -1, max = 1;
    for (int x = min; x <= max; x++) {
      for (int y = min; y <= max; y++) {
        for (int z = min; z <= max; z++) {
          possibleLocations.add(bullsEye.clone().add(x, y, z));
        }
      }
    }

    return possibleLocations.stream()
            .filter(location -> location.getBlock().getType() == Material.WOOL)
            .filter(location -> location.getBlock().getData() == this.defaultBlockDamageValue)
            .collect(Collectors.toCollection(ArrayList::new));
  }

  public boolean contains(Location location) {
    return this.isBullsEye(location) || this.isBlock(location);
  }

  public boolean isBullsEye(Location location) {
    return this.bullsEye.equals(location);
  }

  public boolean isBlock(Location location) {
    return this.blocks.stream().filter(block -> block.equals(location)).count() > 0;
  }

  /**
   * Set the target blocks for the given player
   * FIXME use a diffent way here. Player#sendBlockChange is deprecated
   *
   * @param player the player to set the blocks for
   */
  public void setFinishedFor(Player player) {
    this.setMaterialFor(player, Material.WOOL, this.defaultFinishedBlockDamageValue, this.defaultFinishedBlockDamageValue);
  }

  public void resetBlocksFor(Player player) {
    this.setMaterialFor(player, Material.WOOL, this.defaultBullsEyeDamageValue, this.defaultBlockDamageValue);
  }

  protected void setMaterialFor(Player player, Material material, byte bullsEye, byte blocks) {
    player.sendBlockChange(this.bullsEye, material, bullsEye);
    this.blocks.forEach(block -> player.sendBlockChange(block, material, blocks));
  }
}
