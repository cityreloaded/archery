package de.cityreloaded.archery.command;

import de.cityreloaded.archery.Archery;
import de.paxii.bukkit.commandapi.command.AbstractCommand;

import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

public class HelpCommand extends AbstractCommand {

  @Override
  public String getCommand() {
    return "help";
  }

  @Override
  public String[] getRootCommands() {
    return new String[]{"archery"};
  }

  @Override
  public String getHelp() {
    return "Displays this help message";
  }

  @Override
  public String getPermission() {
    return "Archery.Help";
  }

  @Override
  public boolean executeCommand(CommandSender commandSender, String[] args) {
    commandSender.sendMessage("==========================");
    this.sendMessage(commandSender, "Archery Help: ");
    Archery.getInstance().getCommandApi().getCommandManager().getCommandList().stream()
            .filter(command -> commandSender.hasPermission(command.getPermission()))
            .forEach(command -> this.sendMessage(commandSender, command.getHelp() + " - " + command.getSyntax()));

    return true;
  }

}
