package de.cityreloaded.archery.command.play;

import de.cityreloaded.archery.Archery;
import de.cityreloaded.archery.arena.Arena;
import de.paxii.bukkit.commandapi.command.AbstractCommand;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;
import java.util.Objects;

public class JoinCommand extends AbstractCommand {

  @Override
  public String getCommand() {
    return "join";
  }

  @Override
  public String[] getRootCommands() {
    return new String[]{"archery"};
  }

  @Override
  public String getHelp() {
    return "Joins an Arena";
  }

  @Override
  public String getSyntax() {
    return "join <name>";
  }

  @Override
  public String getPermission() {
    return "Archery.Join";
  }

  @Override
  public boolean isPlayerOnly() {
    return true;
  }

  @Override
  public boolean executeCommand(CommandSender commandSender, String[] args) {
    if (args.length >= 1) {
      Player player = Bukkit.getPlayer(commandSender.getName());
      String arenaName = args[0];
      if (Archery.getArenaManager().isPlayingInArena(player)) {
        this.sendMessage(commandSender, "You are already in another Arena!");
        return true;
      }
      // FIXME: do a proper implementation
      if (Arrays.stream(player.getInventory().getContents()).filter(itemStack -> itemStack == null || itemStack.getType() == Material.AIR).count() < 9) {
        this.sendMessage(commandSender, "You need at least 9 free inventory slots to participate!");
        return true;
      }
      if (Archery.getArenaManager().isArenaRegistered(arenaName)) {
        Arena arena = Archery.getArenaManager().getArena(arenaName);
        if (arena.addPlayer(player)) {
          player.teleport(arena.getLobby());
          this.sendMessage(commandSender, String.format("You are now in the queue for the Arena %s!", arena.getName()));
        } else {
          this.sendMessage(commandSender, String.format("The Arena %s is already full.", arena.getName()));
        }
      } else {
        this.sendMessage(commandSender, String.format("There is no Arena with the name \"%s\".", arenaName));
      }

      return true;
    }

    return false;
  }

}
