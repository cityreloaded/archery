package de.cityreloaded.archery.command.play;

import de.cityreloaded.archery.Archery;
import de.cityreloaded.archery.arena.Arena;
import de.paxii.bukkit.commandapi.command.AbstractCommand;

import org.bukkit.command.CommandSender;

import java.util.List;

public class ListCommand extends AbstractCommand {

  @Override
  public String getCommand() {
    return "list";
  }

  @Override
  public String[] getRootCommands() {
    return new String[]{"archery"};
  }

  @Override
  public String getHelp() {
    return "Lists all Arenas";
  }

  @Override
  public String getSyntax() {
    return "list";
  }

  @Override
  public String getPermission() {
    return "Archery.List";
  }

  @Override
  public boolean isPlayerOnly() {
    return false;
  }

  @Override
  public boolean executeCommand(CommandSender commandSender, String[] args) {
    List<Arena> arenas = Archery.getArenaManager().getAll();

    if (arenas.size() > 0) {
      commandSender.sendMessage("==========================");
      this.sendMessage(commandSender, "Available Arenas:");
      arenas.forEach(arena -> {
        // TODO: add clickable join button to chat message
        String status;
        if (arena.isStarted()) {
          status = String.format("ingame - %d of %d players", arena.getPlayerCount(), arena.getMaximumPlayers());
        } else {
          status = String.format("waiting - %d of %d required players", arena.getPlayerCount(), arena.getMinimumPlayers());
        }
        commandSender.sendMessage(arena.getName() + ": " + status);
      });
    } else {
      this.sendMessage(commandSender, "There currently aren't any Arenas defined.");
    }

    return true;
  }

}
