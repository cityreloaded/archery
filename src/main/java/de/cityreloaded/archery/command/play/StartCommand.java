package de.cityreloaded.archery.command.play;

import de.cityreloaded.archery.Archery;
import de.cityreloaded.archery.arena.Arena;
import de.paxii.bukkit.commandapi.command.AbstractCommand;

import org.bukkit.command.CommandSender;

public class StartCommand extends AbstractCommand {

  @Override
  public String getCommand() {
    return "start";
  }

  @Override
  public String[] getRootCommands() {
    return new String[]{"archery"};
  }

  @Override
  public String getHelp() {
    return "Starts an Arena";
  }

  @Override
  public String getSyntax() {
    return "start <name>";
  }

  @Override
  public String getPermission() {
    return "Archery.Start";
  }

  @Override
  public boolean executeCommand(CommandSender commandSender, String[] args) {
    if (args.length >= 1) {
      String arenaName = args[0];
      if (Archery.getArenaManager().isArenaRegistered(arenaName)) {
        Arena arena = Archery.getArenaManager().getArena(arenaName);
        if (arena.canStart() && arena.start()) {
          this.sendMessage(commandSender, String.format("The Arena %s was started.", arena.getName()));
        } else {
          String message;
          if (arena.isStarted()) {
            message = "The Arena has already started.";
          } else if (arena.getSpawn() == null) {
            message = String.format("The Spawn of the Arena is not set. Use \"/archery setspawn %s\".", arena.getName());
          } else if (arena.getLobby() == null) {
            message = String.format("The Lobby of the Arena is not set. Use \"/archery setlobby %s\".", arena.getName());
          } else if (arena.getTargetList().size() == 0) {
            message = "The Arena does not have any targets defined.";
          } else if (arena.getPlayerCount() < arena.getMinimumPlayers()) {
            message = String.format("The Arena can't start as there aren't enough players in the Arena at the moment. (%d of %d)", arena.getPlayerCount(), arena.getMinimumPlayers());
          } else {
            message = "The Arena can't start for an unknown reason. This should be reported as a bug.";
          }
          this.sendMessage(commandSender, message);
        }
      } else {
        this.sendMessage(commandSender, String.format("There is no Arena with the name \"%s\".", arenaName));
      }

      return true;
    }

    return false;
  }

}
