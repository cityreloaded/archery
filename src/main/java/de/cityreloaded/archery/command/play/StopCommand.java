package de.cityreloaded.archery.command.play;

import de.cityreloaded.archery.Archery;
import de.cityreloaded.archery.arena.Arena;
import de.paxii.bukkit.commandapi.command.AbstractCommand;

import org.bukkit.command.CommandSender;

public class StopCommand extends AbstractCommand {

  @Override
  public String getCommand() {
    return "stop";
  }

  @Override
  public String[] getRootCommands() {
    return new String[]{"archery"};
  }

  @Override
  public String getHelp() {
    return "Stops an Arena";
  }

  @Override
  public String getSyntax() {
    return "stop <name>";
  }

  @Override
  public String getPermission() {
    return "Archery.Stop";
  }

  @Override
  public boolean executeCommand(CommandSender commandSender, String[] args) {
    if (args.length >= 1) {
      String arenaName = args[0];
      if (Archery.getArenaManager().isArenaRegistered(arenaName)) {
        Arena arena = Archery.getArenaManager().getArena(arenaName);
        if (arena.canStop() && arena.stop()) {
          this.sendMessage(commandSender, String.format("The Arena %s was stopped.", arena.getName()));
        } else {
          String message;
          if (!arena.isStarted()) {
            message = "The Arena has not started yet.";
          } else {
            message = "The Arena can't be stopped for an unknown reason. This should be reported as a bug.";
          }
          this.sendMessage(commandSender, message);
        }
      } else {
        this.sendMessage(commandSender, String.format("There is no Arena with the name \"%s\".", arenaName));
      }

      return true;
    }

    return false;
  }

}
