package de.cityreloaded.archery.command.setup;

import de.cityreloaded.archery.Archery;
import de.cityreloaded.archery.arena.Arena;
import de.cityreloaded.archery.service.ArenaService;
import de.paxii.bukkit.commandapi.command.AbstractCommand;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CreateCommand extends AbstractCommand {

  @Override
  public String getCommand() {
    return "create";
  }

  @Override
  public String[] getRootCommands() {
    return new String[]{"archery"};
  }

  @Override
  public String getHelp() {
    return "Creates a new Arena";
  }

  @Override
  public String getSyntax() {
    return "create <name> <payment>";
  }

  @Override
  public String getPermission() {
    return "Archery.Create";
  }

  @Override
  public boolean isPlayerOnly() {
    return true;
  }

  @Override
  public boolean executeCommand(CommandSender commandSender, String[] args) {
    if (args.length >= 2) {
      Player player = Bukkit.getPlayer(commandSender.getName());
      String arenaName = args[0];
      if (Archery.getArenaManager().isArenaRegistered(arenaName)) {
        this.sendMessage(commandSender, String.format("There is already an Arena with the name %s defined.", arenaName));
        return true;
      }
      int payment;
      try {
        payment = Integer.parseInt(args[1]);
      } catch (NumberFormatException e) {
        this.sendMessage(commandSender, "Payment has to be numeric!");
        return false;
      }
      Arena arena = new Arena(arenaName, payment, player.getLocation());
      ArenaService.createArena(player, arena);
      this.sendMessage(commandSender, "Use right and left click to mark the target area.");
      this.sendMessage(commandSender, String.format("Use \"/archery setlobby %s\" to set the lobby of the Arena.", arena.getName()));
      this.sendMessage(commandSender, String.format("Use \"/archery finishsetup %s\" to complete the setup of the Arena.", arena.getName()));

      return true;
    }

    return false;
  }

}
