package de.cityreloaded.archery.command.setup;

import de.cityreloaded.archery.arena.Arena;
import de.cityreloaded.archery.service.ArenaService;
import de.paxii.bukkit.commandapi.command.AbstractCommand;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class FinishSetupCommand extends AbstractCommand {

  @Override
  public String getCommand() {
    return "finishsetup";
  }

  @Override
  public String[] getRootCommands() {
    return new String[]{"archery"};
  }

  @Override
  public String getHelp() {
    return "Finishes the creation of an Arena";
  }

  @Override
  public String getSyntax() {
    return "finishsetup";
  }

  @Override
  public boolean isPlayerOnly() {
    return true;
  }

  @Override
  public String getPermission() {
    return "Archery.Create";
  }

  @Override
  public boolean executeCommand(CommandSender commandSender, String[] args) {
    Player player = Bukkit.getPlayer(commandSender.getName());
    if (ArenaService.isCreatingArena(player)) {
      Arena arena = ArenaService.getArenaByCreator(player);

      if (arena.canFinish()) {
        ArenaService.finishArena(player);
        this.sendMessage(commandSender, String.format("The Arena %s is now ready for action!", arena.getName()));
      } else {
        if (arena.getLobby() == null) {
          this.sendMessage(commandSender, String.format("You did not set the Arena lobby yet. Use \"/archery setlobby %s\" to set your current Location as lobby.", arena.getName()));
        } else if (arena.getTargetList().size() == 0) {
          this.sendMessage(commandSender, "You did not define any targets for the Arena yet. Use left click to mark blocks as bull's eyes.");
        } else {
          this.sendMessage(commandSender, "Unknown Error. Arena could not be finished. This should be reported as a bug!");
        }
      }

      return true;
    }

    this.sendMessage(commandSender, "You are currently not creating an Arena!");

    return true;
  }

}
