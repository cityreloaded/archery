package de.cityreloaded.archery.command.setup;

import de.cityreloaded.archery.Archery;
import de.cityreloaded.archery.arena.Arena;
import de.paxii.bukkit.commandapi.command.AbstractCommand;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SetLobbyCommand extends AbstractCommand {

  @Override
  public String getCommand() {
    return "setlobby";
  }

  @Override
  public String[] getRootCommands() {
    return new String[]{"archery"};
  }

  @Override
  public String getHelp() {
    return "Sets the lobby of an Arena";
  }

  @Override
  public String getSyntax() {
    return "setlobby <name>";
  }

  @Override
  public String getPermission() {
    return "Archery.SetLobby";
  }

  @Override
  public boolean isPlayerOnly() {
    return true;
  }

  @Override
  public boolean executeCommand(CommandSender commandSender, String[] args) {
    if (args.length >= 1) {
      Player player = Bukkit.getPlayer(commandSender.getName());
      String arenaName = args[0];
      if (Archery.getArenaManager().isArenaRegistered(arenaName)) {
        Arena arena = Archery.getArenaManager().getArena(arenaName, true);
        arena.setLobby(player.getLocation());
        this.sendMessage(commandSender, String.format("The Lobby of the Arena %s has been set!", arena.getName()));
      } else {
        this.sendMessage(commandSender, String.format("There is no Arena with the name \"%s\"", arenaName));
      }

      return true;
    }

    return false;
  }

}
