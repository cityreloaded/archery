package de.cityreloaded.archery.inventory;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.PlayerInventory;

import java.util.HashMap;
import java.util.UUID;

public class InventoryManager {

  private HashMap<UUID, StorageInventory> inventories;

  public InventoryManager() {
    this.inventories = new HashMap<>();
  }

  /**
   * @param offlinePlayer the player to search an inventory for
   * @return true if this InventoryManager has an inventory stored for the given player
   */
  public boolean hasInventoryFor(OfflinePlayer offlinePlayer) {
    return this.inventories.containsKey(offlinePlayer.getUniqueId());
  }

  /**
   * Convenience method
   *
   * @param player Player to store inventory for
   */
  public void storeInventory(Player player) {
    this.storeInventory(player, player.getInventory());
  }

  /**
   * Stores the inventory for the given player so it can later be retrieved
   *
   * @param player    the player to store the inventory for
   * @param inventory the inventory to store for the given player
   */
  public void storeInventory(OfflinePlayer player, PlayerInventory inventory) {
    this.inventories.put(player.getUniqueId(), this.cloneInventory(inventory));
  }

  /**
   * Restores the saved inventory for the given player
   *
   * @param player the player we stored the inventory for
   */
  public void restoreInventory(Player player) {
    if (this.hasInventoryFor(player)) {
      StorageInventory storageInventory = this.inventories.get(player.getUniqueId());
      player.getInventory().setArmorContents(storageInventory.getArmorContents());
      player.getInventory().setContents(storageInventory.getContents());
    }
  }

  /**
   * "Clone" the given PlayerInventory to a StorageInventory
   *
   * @param inventory the inventory to "clone"
   * @return the "cloned" StorageInventory
   */
  private StorageInventory cloneInventory(PlayerInventory inventory) {
    return new StorageInventory(inventory.getArmorContents(), inventory.getContents());
  }

}
