package de.cityreloaded.archery.inventory;

import org.bukkit.inventory.ItemStack;

import lombok.Data;

@Data
public class StorageInventory {

  private ItemStack[] armorContents;
  private ItemStack[] contents;

  public StorageInventory(ItemStack[] armorContents, ItemStack[] contents) {
    this.armorContents = armorContents;
    this.contents = contents;
  }

}
