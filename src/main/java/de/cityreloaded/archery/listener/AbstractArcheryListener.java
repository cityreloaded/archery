package de.cityreloaded.archery.listener;

import de.cityreloaded.archery.Archery;

import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

abstract public class AbstractArcheryListener implements Listener {

  public void sendMessage(Player player, String message) {
    player.sendMessage(Archery.CHAT_PREFIX + message);
  }

}
