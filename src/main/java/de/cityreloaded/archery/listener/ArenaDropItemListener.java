package de.cityreloaded.archery.listener;

import de.cityreloaded.archery.Archery;

import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerDropItemEvent;

public class ArenaDropItemListener extends AbstractArcheryListener {

  @EventHandler
  public void itemDropHandler(PlayerDropItemEvent dropItemEvent) {
    if (Archery.getArenaManager().isPlayingInArena(dropItemEvent.getPlayer())) {
      dropItemEvent.setCancelled(true);
    }
  }

}
