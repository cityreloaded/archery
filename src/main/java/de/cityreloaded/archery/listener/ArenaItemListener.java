package de.cityreloaded.archery.listener;

import de.cityreloaded.archery.Archery;
import de.cityreloaded.archery.arena.Arena;
import de.cityreloaded.archery.arena.Attempt;

import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityShootBowEvent;

public class ArenaItemListener extends AbstractArcheryListener {

  @EventHandler
  public void shootBowHandler(EntityShootBowEvent bowEvent) {
    if (!(bowEvent.getEntity() instanceof Player
            && bowEvent.getProjectile() instanceof Arrow)) {
      return;
    }

    Player shooter = (Player) bowEvent.getEntity();
    Arena arena = Archery.getArenaManager().getArenaByPlayer(shooter);
    if (arena == null) {
      return;
    }

    Attempt attempt = arena.getAttempt(shooter);

    if (attempt.getUsedAttempts() < arena.getAllowedAttempts()) {
      attempt.addAttempt();
      // TODO: set bow durability / dragon live thingy?
    } else {
      this.sendMessage(shooter, "You don't have any attempts left!");
      bowEvent.setCancelled(true);
    }
  }

}
