package de.cityreloaded.archery.listener;

import de.cityreloaded.archery.Archery;
import de.cityreloaded.archery.arena.Arena;
import de.cityreloaded.archery.arena.Attempt;
import de.cityreloaded.archery.arena.Target;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.util.BlockIterator;

public class ArenaProjectileListener extends AbstractArcheryListener {

  @EventHandler
  public void projectileHitHandler(ProjectileHitEvent event) {
    if (!(event.getEntity().getShooter() instanceof Player
            && event.getEntity() instanceof Arrow)) {
      return;
    }

    Player shooter = (Player) event.getEntity().getShooter();
    Arena arena = Archery.getArenaManager().getArenaByPlayer(shooter);
    if (arena == null) {
      return;
    }
    Attempt attempt = arena.getAttempt(shooter);

    Arrow arrow = (Arrow) event.getEntity();
    BlockIterator blockIterator = new BlockIterator(arrow.getWorld(), arrow.getLocation().toVector(), arrow.getVelocity().normalize(), 0, 4);
    Block hitBlock = null;
    while (blockIterator.hasNext()) {
      hitBlock = blockIterator.next();
      if (hitBlock.getType() != Material.AIR) {
        break;
      }
    }

    if (hitBlock == null) {
      System.out.println("COULD NOT DETERMINE BLOCK");
      return;
    }

    Target target = arena.getTargetByLocation(hitBlock.getLocation());
    if (target != null) {
      if (attempt.getFinishedTargets().contains(target)) {
        this.sendMessage(shooter, "You have already finished that target!");
        return;
      }
      if (target.isBullsEye(hitBlock.getLocation())) {
        attempt.addBullsEyeHit(target);
        this.sendMessage(shooter, "Bull's Eye! + 10 Points");
        target.setFinishedFor(shooter);
      } else if (target.isBlock(hitBlock.getLocation())) {
        attempt.addBlockHit(target);
        this.sendMessage(shooter, "Block hit! + 5 Points");
        target.setFinishedFor(shooter);
      }
    }

    if (arena.canStop()) {
      arena.stop();
    }
  }

}
