package de.cityreloaded.archery.listener;

import de.cityreloaded.archery.Archery;
import de.cityreloaded.archery.arena.Arena;
import de.cityreloaded.archery.arena.Target;
import de.cityreloaded.archery.service.ArenaService;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public class ArenaTargetListener extends AbstractArcheryListener {

  @EventHandler
  public void playerClickHandler(PlayerInteractEvent interactEvent) {
    Player player = interactEvent.getPlayer();
    if (!ArenaService.isCreatingArena(player)) {
      return;
    }
    interactEvent.setCancelled(true);
    Arena arena = ArenaService.getArenaByCreator(player);
    if (arena.getTargetList().size() >= 10) {
      this.sendMessage(player, String.format("The Arena %s already has 10 targets defined!", arena.getName()));
      return;
    }
    if (interactEvent.getAction() == Action.LEFT_CLICK_BLOCK) {
      Location location = interactEvent.getClickedBlock().getLocation();

      if (arena.getTargetList().stream().map(Target::getBullsEye).filter(bullsEye -> bullsEye.equals(location)).count() > 0) {
        this.sendMessage(player, String.format("This bull's eye is already a target for the Arena %s.", arena.getName()));
        return;
      }
      Target target = new Target(location);
      arena.getTargetList().add(target);
      this.sendMessage(player, String.format("Target was added to Arena %s.", arena.getName()));
    }
  }

}
