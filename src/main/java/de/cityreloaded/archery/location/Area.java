package de.cityreloaded.archery.location;

import org.bukkit.Location;

import lombok.Data;

/**
 * Holds an area marked by two Locations
 */
@Data
public class Area {

  private double maxX;
  private double maxZ;
  private double maxY;
  private double minX;
  private double minZ;
  private double minY;

  /**
   * Start location of the area
   */
  private Location start;

  /**
   * End location of the area
   */
  private Location end;

  public Area() {
    this(null, null);
  }

  public Area(Location start, Location end) {
    this.start = start;
    this.end = end;

    this.setup();
  }

  private void setup() {
    if (this.start == null || this.end == null) {
      return;
    }
    this.maxX = Math.max(this.start.getX(), this.end.getX());
    this.minX = Math.min(this.start.getX(), this.end.getX());
    this.maxY = Math.max(this.start.getY(), this.end.getY());
    this.minY = Math.min(this.start.getY(), this.end.getY());
    this.maxZ = Math.max(this.start.getZ(), this.end.getZ());
    this.minZ = Math.min(this.start.getZ(), this.end.getZ());
  }

  public boolean hasBounds() {
    return this.start != null && this.end != null;
  }

  public boolean contains(Location location) {
    return location.getWorld().equals(this.start.getWorld())
            && location.getX() >= this.minX && location.getX() <= this.maxX
            && location.getY() >= this.minY && location.getY() <= this.maxY
            && location.getZ() >= this.minZ && location.getZ() <= this.maxZ;
  }

  public void setStart(Location location) {
    this.start = location;
    this.setup();
  }

  public void setEnd(Location location) {
    this.end = location;
    this.setup();
  }

}



