package de.cityreloaded.archery.persistence;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import de.cityreloaded.archery.Archery;
import de.cityreloaded.archery.arena.Arena;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;
import java.util.stream.Collectors;

public class ArenaStorage {

  private static Gson gson = new GsonBuilder().setPrettyPrinting().create();
  private static File arenaFile = new File(Archery.getInstance().getDataFolder(), "arenas.json");

  public static void saveArenas(List<Arena> arenaList) throws IOException {
    if (!arenaFile.exists()) {
      arenaFile.createNewFile();
    }
    StorageArenaBucket arenaBucket = new StorageArenaBucket(
            arenaList.stream().map(StorageArena::new).collect(Collectors.toCollection(ArrayList::new))
    );

    String jsonString = gson.toJson(arenaBucket);
    PrintWriter printWriter = new PrintWriter(arenaFile);
    printWriter.print(jsonString);
    printWriter.close();
  }

  public static List<Arena> loadArenas() throws IOException {
    if (!arenaFile.exists()) {
      arenaFile.createNewFile();
      return new ArrayList<>();
    }

    Scanner scanner = new Scanner(arenaFile);
    StringBuilder stringBuilder = new StringBuilder();
    while (scanner.hasNextLine()) {
      stringBuilder.append(scanner.nextLine());
    }
    scanner.close();
    String jsonString = stringBuilder.toString();

    if (jsonString.trim().isEmpty()) {
      return new ArrayList<>();
    }
    StorageArenaBucket arenaBucket = gson.fromJson(stringBuilder.toString(), StorageArenaBucket.class);

    return arenaBucket.getArenaList().stream()
            .map(storageArena -> {
              try {
                return storageArena.getRealArena();
              } catch (Exception e) {
                e.printStackTrace();
              }
              return null;
            })
            .filter(Objects::nonNull)
            .collect(Collectors.toCollection(ArrayList::new));
  }

}
