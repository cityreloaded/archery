package de.cityreloaded.archery.persistence;

import de.cityreloaded.archery.arena.Arena;
import de.cityreloaded.archery.arena.Target;

import org.bukkit.Bukkit;
import org.bukkit.Location;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import lombok.Data;

@Data
public class StorageArena {

  private String name;
  private int payment;
  private StorageLocation spawn;
  private StorageLocation lobby;
  private List<StorageTarget> targetList;
  private int minimumPlayers;
  private int maximumPlayers;

  public StorageArena(Arena arena) {
    this.name = arena.getName();
    this.payment = arena.getPayment();
    this.spawn = new StorageLocation(arena.getSpawn());
    this.lobby = new StorageLocation(arena.getLobby());
    this.targetList = arena.getTargetList().stream().map(StorageTarget::new).collect(Collectors.toCollection(ArrayList::new));
    this.minimumPlayers = arena.getMinimumPlayers();
    this.maximumPlayers = arena.getMaximumPlayers();
  }

  public StorageArena(String name, int payment, StorageLocation spawn, StorageLocation lobby, List<StorageTarget> targetList, int minimumPlayers, int maximumPlayers) {
    this.name = name;
    this.payment = payment;
    this.spawn = spawn;
    this.lobby = lobby;
    this.targetList = targetList;
    this.minimumPlayers = minimumPlayers;
    this.maximumPlayers = maximumPlayers;
  }

  public Arena getRealArena() throws Exception {
    Arena arena = new Arena(this.name, this.payment, this.getSpawn().getRealLocation(), this.targetList.stream()
            .map(storageTarget -> {
              try {
                return storageTarget.getRealTarget();
              } catch (Exception e) {
                e.printStackTrace();
              }
              return null;
            })
            .filter(Objects::nonNull)
            .collect(Collectors.toCollection(ArrayList::new)));
    arena.setLobby(this.getLobby().getRealLocation());
    arena.setMinimumPlayers(this.minimumPlayers);
    arena.setMaximumPlayers(this.maximumPlayers);

    return arena;
  }

  @Data
  class StorageTarget {
    private StorageLocation bullsEye;
    private List<StorageLocation> blocks;

    public StorageTarget(Target target) {
      this.bullsEye = new StorageLocation(target.getBullsEye());
      this.blocks = target.getBlocks().stream().map(StorageLocation::new).collect(Collectors.toCollection(ArrayList::new));
    }

    public StorageTarget(StorageLocation bullsEye, List<StorageLocation> blocks) {
      this.bullsEye = bullsEye;
      this.blocks = blocks;
    }

    public Target getRealTarget() throws Exception {
      return new Target(this.bullsEye.getRealLocation(), this.blocks.stream()
              .map(storageLocation -> {
                try {
                  return storageLocation.getRealLocation();
                } catch (Exception e) {
                  e.printStackTrace();
                }
                return null;
              })
              .filter(Objects::nonNull)
              .collect(Collectors.toCollection(ArrayList::new)));
    }
  }

  @Data
  class StorageLocation {
    private String world;
    private double x;
    private double y;
    private double z;
    private float pitch;
    private float yaw;

    public StorageLocation(Location location) {
      this.world = location.getWorld().getName();
      this.x = location.getX();
      this.y = location.getY();
      this.z = location.getZ();
      this.pitch = location.getPitch();
      this.yaw = location.getYaw();
    }

    public StorageLocation(String world, double x, double y, double z, float pitch, float yaw) {
      this.world = world;
      this.x = x;
      this.y = y;
      this.z = z;
      this.pitch = pitch;
      this.yaw = yaw;
    }

    public Location getRealLocation() throws Exception {
      if (Bukkit.getWorld(this.world) != null) {
        return new Location(
                Bukkit.getWorld(this.world),
                this.x,
                this.y,
                this.z,
                this.yaw,
                this.pitch
        );
      } else {
        throw new Exception(String.format("World %s was not found.", this.world));
      }
    }
  }
}
