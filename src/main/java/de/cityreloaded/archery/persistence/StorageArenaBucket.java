package de.cityreloaded.archery.persistence;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class StorageArenaBucket {
  private List<StorageArena> arenaList = new ArrayList<>();

  public StorageArenaBucket(List<StorageArena> arenaList) {
    this.arenaList = arenaList;
  }
}
