package de.cityreloaded.archery.service;

import de.cityreloaded.archery.Archery;
import de.cityreloaded.archery.arena.Arena;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.UUID;

public class ArenaService {

  /**
   * Holds a map of uuids to arenas. Used to determine who's setting up an arena
   */
  private static HashMap<UUID, Arena> playersToArenaMap = new HashMap<>();

  /**
   * @param player the player to check for
   * @return true if the player is currently setting up an arena
   */
  public static boolean isCreatingArena(OfflinePlayer player) {
    return playersToArenaMap.containsKey(player.getUniqueId());
  }

  /**
   * @param player the player to get the arena for
   * @return the arena the given player is currently creating
   */
  public static Arena getArenaByCreator(OfflinePlayer player) {
    return playersToArenaMap.get(player.getUniqueId());
  }

  /**
   * @param arena the arena to check for
   * @return true if the arena is currently being set up
   */
  public static boolean isInSetup(Arena arena) {
    return playersToArenaMap.values().contains(arena);
  }

  /**
   * @param player the player to set as creator
   * @param arena  the arena to set the creator for
   */
  public static void createArena(Player player, Arena arena) {
    playersToArenaMap.put(player.getUniqueId(), arena);
    Archery.getArenaManager().registerArena(arena);
    arena.setInSetup(true);
  }

  /**
   * @param player the creator of the arena to finish
   */
  public static void finishArena(OfflinePlayer player) {
    if (isCreatingArena(player)) {
      Arena arena = playersToArenaMap.get(player.getUniqueId());
      arena.finish();
      playersToArenaMap.remove(player.getUniqueId());
    }
  }

}
